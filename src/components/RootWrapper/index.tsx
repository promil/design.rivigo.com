import React from 'react'

//TODO import global styles here
import '../theme/index.scss'

interface RootWrapperProps {
  /** Site content */
  children: React.ReactNode
}

const RootWrapper = ({ children }: RootWrapperProps) => {
  return <div className="root-wrapper">{children}</div>
}

export default RootWrapper
