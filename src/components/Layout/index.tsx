import React from 'react'
import { Link } from 'gatsby'
import { ReactComponent as LogoIcon } from '@/icons/logo.svg'
import { ReactComponent as GithubIcon } from '@/icons/github.svg'
import { Button } from '../button'

interface LayoutProps {
  /** Page content */
  children: React.ReactNode
}

const Layout = ({ children }: LayoutProps) => {
  return (
    <div>
      <div>
        <LogoIcon style={{ width: '10px' }} />
        <Link to="/">Gatsby Starter Vadyan</Link>
        <Button flat={false} appearance="primary" variant="iconBtn"></Button>
        <div>
          <Link to="/" activeClassName="active">
            Quick start
          </Link>
          <Link to="/showcase/" activeClassName="active">
            Showcase
          </Link>
        </div>
      </div>
      <div>{children}</div>
      <div>
        <a href="https://github.com/p1t1ch/gatsby-starter" target="_blank" rel="noopener noreferrer">
          <GithubIcon width={48} title="Project page on Github" />
        </a>
      </div>
    </div>
  )
}

export default Layout
