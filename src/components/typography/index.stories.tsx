// ButtonR.stories.tsx

import React from 'react'
import { Typography, TypographyProps } from '.'
import { Story, Meta } from '@storybook/react/types-6-0'
import './story.scss'

const TypographyStories = {
  title: 'Typography',
  component: Typography,
} as Meta

// We create a “template” of how args map to rendering
const Template: Story<TypographyProps> = args => {
  return <Typography {...args}></Typography>
}

export const Primary = Template.bind({})
Primary.args = {
  className: '',
  children: 'A Visual Type Scale',
}

export default TypographyStories
