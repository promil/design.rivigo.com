import React from 'react'

export type ButtonAppearance = 'primary' | 'warn'
export type ButtonVariant = 'default' | 'iconBtn' | 'iconRoundBtn'

export interface ButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  flat?: boolean
  invert?: boolean
  appearance?: ButtonAppearance
  variant?: ButtonVariant
  icon?: SvgrComponent
}

const appearanceToClassMap = {
  primary: 'appearance-btn-primary',
  warn: 'appearance-btn-warn',
}

const variantToClassMap = {
  default: 'variant-btn-default',
  iconBtn: 'variant-btn-icon',
  iconRoundBtn: 'variant-btn-icon-round',
}

function Button(props: ButtonProps) {
  const {
    flat = false,
    appearance = 'primary',
    variant = 'default',
    icon,
    className = '',
    invert = false,
    ...rest
  } = props

  const classList = `btn ${appearanceToClassMap[appearance]} ${variantToClassMap[variant]} ${className} ${
    flat ? 'type-flat-btn' : ''
  }  ${invert ? 'btn-inverted' : ''}`

  const shouldRenderLabel = variant === 'default'

  return (
    <button className={`${classList}`} {...rest}>
      {shouldRenderLabel ? props.children : icon ? icon({}) : null}
    </button>
  )
}

export { Button }
