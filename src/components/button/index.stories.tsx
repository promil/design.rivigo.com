// ButtonR.stories.tsx

import React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'
import { Button, ButtonProps } from '.'
import { ReactComponent as Trash } from '@/icons/trash.svg'

const ButtonStories = {
  title: 'Button',
  component: Button,
} as Meta

// We create a “template” of how args map to rendering
const Template: Story<ButtonProps> = args => <Button {...args}></Button>

export const Primary = Template.bind({})
Primary.args = {
  children: 'Click me !',
}

export const Warn = Template.bind({})
Warn.args = { ...Primary.args, appearance: 'warn', flat: false }

export const FlatButton = Template.bind({})
FlatButton.args = { ...Primary.args, flat: true }

export const IconButton = Template.bind({})
IconButton.args = { ...Primary.args, icon: Trash, variant: 'iconBtn' }

export const RoundIconBtn = Template.bind({})
RoundIconBtn.args = { ...Primary.args, icon: Trash, variant: 'iconRoundBtn' }

export default ButtonStories
