import React from 'react'

export interface TypographyProps extends React.HTMLAttributes<HTMLElement> {
  className?: string
}

function Typography(props: TypographyProps) {
  const { children, className = '' } = props

  return (
    <>
      <h1 className={className}>{children}</h1>
      <h2 className={className}>{children}</h2>
      <h3 className={className}>{children}</h3>
      <h4 className={className}>{children}</h4>
      <h5 className={className}>{children}</h5>
      <h6 className={className}>{children}</h6>
      <p className={className}>{children}</p>

      <br></br>
      <p className={className}>
        Lorem ipsum dolor sit amet, consectetur adipiscing Lorem ipsum dolor sit, amet consectetur adipisicing elit.
        Veritatis, facilis, est quae vero quos ratione repellendus tempora sit dicta nihil consequuntur velit fugit
        facere id ducimus impedit pariatur nobis odio.
      </p>
    </>
  )
}

export { Typography }
