import React from 'react'
import Layout from '@/components/Layout'
import Seo from '@/components/Seo'

const ShowcasePage = () => {
  return (
    <Layout>
      <Seo title="Showcase" />
      <h1>Showcase</h1>
      <h2>Static data with Gatsby Image</h2>
      <h2>Dynamic data with Apollo Client</h2>
    </Layout>
  )
}

export default ShowcasePage
